﻿import { observable, action, computed, configure, runInAction, ObservableMap } from 'mobx';
import { createContext, SyntheticEvent } from 'react';
import { IActivity } from '../models/Activity';
import Activites from '../api/agent';

configure({ 'enforceActions': 'always' })

class ActivityStore {
    @observable activitiesRegistry = new ObservableMap()
    @observable selectedActivity: IActivity | null = null
    @observable target = ''

    @observable loadingInitial = false
    @observable submitting = false

    @computed get activitiesByDate() {
        return this.getActivitiesGroupedByDate(Array.from(this.activitiesRegistry.values()))
    }

    getActivitiesGroupedByDate(activities: IActivity[]) {
        let sortedActivities = activities.sort(
            (a, b) => Date.parse(a.date) - Date.parse(b.date)
        );

        return Object.entries(sortedActivities.reduce((activities, activity) => {
            const date = activity.date.split('T')[0];
            activities[date] = activities[date] ? [...activities[date], activity] : [activity]
            return activities
        }, {} as { [key: string]: IActivity[] }));
    }

    @action loadActivities = async () => {
        this.loadingInitial = true
        try {
            const activities = await Activites.list()
            runInAction(() => {
                activities.forEach(activity => {
                    activity.date = activity.date.split('.')[0]
                    this.activitiesRegistry.set(activity.id, activity)
                })
            })
        } catch (error) {
            console.log(error)
        } finally {
            runInAction(() => this.loadingInitial = false)
        }
    }

    @action createActivity = async (activity: IActivity) => {
        this.submitting = true
        try {
            await Activites.create(activity)
            runInAction(() => {
                this.activitiesRegistry.set(activity.id, activity)
                this.selectedActivity = activity
            })
        } catch (error) {
            console.log(error)
        } finally {
            runInAction(() => this.submitting = false)
        }
    }

    @action editActivity = async (activity: IActivity) => {
        this.submitting = true
        try {
            await Activites.update(activity)
            runInAction(() => {
                this.activitiesRegistry.set(activity.id, activity)
                this.selectedActivity = activity
            })
        } catch (error) {
            console.log(error)
        } finally {
            runInAction(()  => this.submitting = false)
        }
    }

    @action deleteActivity = async (e: SyntheticEvent<HTMLButtonElement>, id: string) => {
        this.submitting = true
        this.target = e.currentTarget.name

        try {
            await Activites.delete(id)
            runInAction(() => this.activitiesRegistry.delete(id))
        } catch (error) {
            console.log(error)
        } finally {
            runInAction(() => {
                this.submitting = false
                this.target = ''
            })
        }
    }

    @action loadActivty = async (id: string) => {
        let activity = this.activitiesRegistry.get(id)

        if (activity) {
            this.selectedActivity = activity
        } else {
            this.loadingInitial = true
            activity = await Activites.details(id)
            try {
                runInAction(() => {
                    this.selectedActivity = activity
                })
            } catch (error) {
                console.log(error)
            } finally {
                runInAction(() => {
                    this.loadingInitial = false
                })
            }
        }
    }

    @action resetSelectedActivity = () => {
        this.selectedActivity = null
    }

    @action selectActivty = (id: string) => {
        this.selectedActivity = this.activitiesRegistry.get(id);
    }
}

export default createContext(new ActivityStore())