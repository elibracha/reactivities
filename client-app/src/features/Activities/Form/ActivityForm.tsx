﻿import React, { useState, FormEvent, useContext, useEffect } from 'react';
import { Form, Segment, Button, Grid } from 'semantic-ui-react';
import { v4 as uuid } from 'uuid';

import ActivityStore from '../../../app/stores/activityStore';
import { observer } from 'mobx-react-lite';
import { RouteComponentProps } from 'react-router-dom';

interface DetailParmas {
    id: string
}

const ActivityForm: React.FC<RouteComponentProps<DetailParmas>> = ({ match, history }) => {

    const activityStore = useContext(ActivityStore);
    const { resetSelectedActivity, loadActivty, selectedActivity, createActivity, submitting, editActivity } = activityStore
    const [activity, setActivity] = useState({
        id: '',
        title: '',
        description: '',
        category: '',
        date: '',
        city: '',
        venue: ''
    })

    useEffect(() => {
        if (match.params.id && activity.id.length === 0) {
            loadActivty(match.params.id).then(() => {
                if (selectedActivity)
                    setActivity(selectedActivity!)
            })
        }

        return () => {
            resetSelectedActivity()
        }
    }, [match, activity.id.length, loadActivty, resetSelectedActivity, selectedActivity])


    const handleChange = (event: FormEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const { name, value } = event.currentTarget
        setActivity({ ...activity, [name]: value })
    }

    const handleSubmit = () => {
        if (activity.id.length === 0) {
            let newActivtiy = {
                ...activity,
                id: uuid()
            }
            createActivity(newActivtiy).then(() => history.push(`/activities/${activity.id}`))
        } else {
            editActivity(activity).then(() => history.push(`/activities/${activity.id}`))
        }
    }

    return (
        <Grid>
            <Grid.Column width={10}>
                <Segment clearing>
                    <Form>
                        <Form.Input placeholder="Title" value={activity.title} name="title" onChange={handleChange} />
                        <Form.TextArea rows={2} placeholder="Description" name="description" onChange={handleChange} value={activity.description} />
                        <Form.Input placeholder="Category" name="category" onChange={handleChange} value={activity.category} />
                        <Form.Input placeholder="City" name="city" onChange={handleChange} value={activity.city} />
                        <Form.Input type="datetime-local" name="date" onChange={handleChange} placeholder="Date" value={activity.date} />
                        <Form.Input placeholder="Venue" name="venue" onChange={handleChange} value={activity.venue} />
                        <Button onClick={handleSubmit} loading={submitting} floated="right" positive type="submit" content="Submit" />
                        <Button onClick={() => history.push('/activities')} floated="right" type="button" content="Cancel" />
                    </Form>
                </Segment>
            </Grid.Column>
        </Grid>
    );
}

export default observer(ActivityForm);