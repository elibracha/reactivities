﻿import React, { useContext, useEffect } from 'react';
import { Grid } from 'semantic-ui-react';
import { observer } from 'mobx-react-lite';
import ActivityStore from '../../../app/stores/activityStore';
import { RouteComponentProps } from 'react-router-dom';
import LoadingComponent from '../../../app/layout/LoadingComponent';
import ActivityDetailedHeader from './ActivityDetailedHeader';
import ActivityDetailedInfo from './ActivityDetailedInfo';
import ActivityDetailedChat from './ActivityDetailedChat';
import ActivityDetailedSideBar from './ActivityDetailedSideBar';

interface DetailProps {
    id: string
}

const ActivityDetail: React.FC<RouteComponentProps<DetailProps>> = ({ match }) => {

    const activityStore = useContext(ActivityStore);
    const { selectedActivity, loadActivty, loadingInitial } = activityStore

    useEffect(() => {
        loadActivty(match.params.id)
    }, [match, loadActivty])

    if (loadingInitial || !selectedActivity) return <LoadingComponent content="Loading activity..." />

    return (
        <Grid>
            <Grid.Column width={10}>
                <ActivityDetailedHeader activity={selectedActivity} />
                <ActivityDetailedInfo activity={selectedActivity} />
                <ActivityDetailedChat />
            </Grid.Column>
            <Grid.Column width={6}>
                <ActivityDetailedSideBar />
            </Grid.Column>
        </Grid>
    )
}

export default observer(ActivityDetail);