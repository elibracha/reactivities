﻿import React from 'react';
import { IActivity } from '../../../app/models/Activity';
import { Item, Button, Segment, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

interface IProps {
    activity: IActivity
}

const ActivityListItem: React.FC<IProps> = ({ activity }) => {
    return (
        <Segment.Group>
            <Segment>
                <Item.Group>
                    <Item>
                        <Item.Image size='tiny' circular src='/assets/user.png' />
                        <Item.Content>
                            <Item.Header as='a'>{activity.title}</Item.Header>
                            <Item.Description>Hosted by Bob</Item.Description>
                        </Item.Content>
                    </Item>
                </Item.Group>
            </Segment>
            <Segment>
                <Icon name='clock' /> {activity.date}
                <Icon name='marker' /> {activity.venue}, {activity.city}
            </Segment>
            <Segment secondary>Attendees will go here</Segment>
            <Segment clearing>
                <span>{activity.description}</span>
                <Button
                    as={Link} exact="true" to={`/activities/${activity.id}`}
                    floated="right"
                    content="view"
                    color="blue" />
            </Segment>
        </Segment.Group>
    )
}

export default ActivityListItem;