﻿import React from 'react';
import { Menu, Container, Button } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

const NavBar: React.FC = () => {

    return (
        <Menu fixed='top' inverted>
            <Container>
                <Menu.Item as={NavLink} exact to='/'>
                    <img src="/assets/logo.png" alt="logo" style={{ marginRight: "10px" }} />
                    Reactivities
                </Menu.Item>
                <Menu.Item name='Activities' as={NavLink} exact to='/activities' />
                <Menu.Item as={NavLink} exact to='/createActivity' >
                    <Button positive content="Create Activity" />
                </Menu.Item>
            </Container>
        </Menu>
    )
}

export default NavBar;