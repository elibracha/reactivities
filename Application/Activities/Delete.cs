﻿using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Activities
{
    public class Delete
    {
        public class Command: IRequest
        {
            public Guid Id{ get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var activity = await _context.Activities.AnyAsync(a => a.Id == request.Id);

                if (activity)
                {
                    _context.Activities.Remove(new Activity { Id = request.Id });

                    var success = await _context.SaveChangesAsync() > 0;

                    if (success) return Unit.Value;
                }

                throw new Exception("Problem could not delete activity.");
            }
        }
    }
}
